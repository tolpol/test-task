/* declare variable if need and set values*/
if (window.scripts === undefined) {
    let scripts;
}
scripts = document.getElementsByTagName('script');

if (window.adsBlockScript === undefined) {
    let adsBlockScript;
}
adsBlockScript = scripts[scripts.length - 1];

if (window.resourceId === undefined) {
    let resourceId;
}
resourceId = adsBlockScript.dataset.resource;

if (window.adsId === undefined) {
    let adsId;
}
adsId = adsBlockScript.dataset.ads;

if (window.adsWidth === undefined) {
    let adsWidth;
}
adsWidth = adsBlockScript.dataset.width;

if (window.adsHeight === undefined) {
    let adsHeight;
}
adsHeight = adsBlockScript.dataset.height;

if (window.adsColor === undefined) {
    let adsColor;
}
adsColor = adsBlockScript.dataset.backcolor;

/* create ads block */
findBlock = document.getElementById("ads_block_r" + resourceId + "_a" + adsId);
if (findBlock === null) {
    document.write('<div id="ads_block_r' + resourceId + '_a' + adsId + '"></div>');

    /* get ads block */
    if (window.adsBlock === undefined) {
        let adsBlock;
    }
    adsBlock = document.getElementById("ads_block_r" + resourceId + "_a" + adsId);

    /* add style to ads block */
    adsBlock.style.width = adsWidth;
    adsBlock.style.height = adsHeight;
    adsBlock.style.backgroundColor = adsColor;
    adsBlock.style.display = "flex";
    adsBlock.style.alignItems = 'center';
    adsBlock.style.justifyContent = 'center';

    /*  */
    if (window.adsButton === undefined) {
        let adsButton;
    }
    adsButton = document.createElement('button');

    /* add params to ads button */
    adsButton.innerText = "Click me!";
    adsButton.dataset.resource = resourceId;
    adsButton.dataset.ads = adsId;

    adsBlock.appendChild(adsButton);

    /* add click handler */
    adsButton.onclick = function(elem) {
        let resourceId = elem.target.dataset.resource;
        let adsId = elem.target.dataset.ads;

        sendClickStatistics(resourceId, adsId);
    }

    sendShowStatistics(resourceId, adsId);
}

/* send show ads statistics to server */
function sendShowStatistics(resourceId, adsId) {
    let xhr = new XMLHttpRequest();

    let body = 'resource_id=' + encodeURIComponent(resourceId) +
        '&ads_id=' + encodeURIComponent(adsId) +
        '&user_ip=' + encodeURIComponent('ip....') +
        '&country_code=' + encodeURIComponent('country_code....');

    xhr.open('POST', '../show/new', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(body);
}

/* send click ads statistics to server */
function sendClickStatistics(resourceId, adsId) {
    let xhr = new XMLHttpRequest();

    let body = 'resource_id=' + encodeURIComponent(resourceId) +
        '&ads_id=' + encodeURIComponent(adsId) +
        '&user_ip=' + encodeURIComponent('192....') +
        '&country_code=' + encodeURIComponent('country_code....');

    xhr.open('POST', '../click/new', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(body);
}