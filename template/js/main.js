document.addEventListener('DOMContentLoaded', function(){
    highLightElem();

    if (typeof(table) !== "undefined") {
        window.result = table;

        window.tHead = document.getElementById("t-head");
        window.tBody = document.getElementById("t-body");

        window.allCols = new Map([
            ['date', "Дата показа"],
            ['resource_id', "ID интернет ресурса"],
            ['ads_id', "ID рекламного блока"],
            ['country_code', "ISO код страны"],
            ['shows', "Количество показов"],
            ['clicks', "Количество кликов"],
        ]);

        group();

        /* date arrays */
        let years = getNumbers(2010, 2020);
        let months = getNumbers(1, 12);
        let days = getNumbers(1, 31);
        window.monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        /* set date values to selects */
        let minYear = document.getElementById('min_year');
        years.forEach(function (item) {
            addOption(minYear, item)
        });

        let maxYear = document.getElementById('max_year');
        maxYear.innerHTML = minYear.innerHTML;
        let yearCount = years.length;
        maxYear.value = years[yearCount - 1];

        let minMonth = document.getElementById('min_month');
        months.forEach(function (item) {
            addOption(minMonth, item);
        });

        let maxMonth = document.getElementById('max_month');
        maxMonth.innerHTML = minMonth.innerHTML;
        maxMonth.value = '12';

        let minDay = document.getElementById('min_day');
        days.forEach(function (item) {
            addOption(minDay, item);
        });

        let maxDay = document.getElementById('max_day');
        maxDay.innerHTML = minDay.innerHTML;
        maxDay.value = '31';
    }
});

/* return collection of days */
function getNumbers(from, to) {
    let numbers = [];
    let val = '';

    for (let i = from; i <= to; i++) {
        val = i < 10 ? '0' + i : '' + i;

        numbers[i] =  val;
    }

    return numbers;
}

/* return collection of days */
function setDays(elem) {
    let prefix = elem.dataset.name;
    let daysSelect = document.getElementById(prefix + '_day');

    daysSelect.innerHTML = '';

    let monthNumber = Number(elem.value);
    let newDays = getNumbers(1, monthDays[monthNumber - 1]);
    newDays.forEach(function (item, index) {
        addOption(daysSelect, item);
    });
}

/* add option to select */
function addOption(elem, val) {
    let option = document.createElement('option');
    option.value = val;
    option.text = val;

    elem.appendChild(option);
}

/* delete confirm */
function deleteItem () {
    let confirmed = confirm("Элемент будет удалён!");
    if (!confirmed) {
        return false;
    }
}

/* show js code */
function showAdsCode(element) {
    let urlId = element.parentElement.parentElement.children[2].innerHTML;
    let adsId = element.parentElement.parentElement.children[0].innerHTML;

    let codeModal = document.getElementById('code-modal');
    codeModal.style.display = "flex";

    document.getElementById('modal-text').innerHTML = generateAdsCode(urlId, adsId);
}

/* generate ads js code */
function generateAdsCode(urlId, adsId) {
    window.jsCode = '&lt;script type="text/javascript" ' +
        'data-resource="' + urlId +
        '" data-ads="' + adsId +
        '" data-width="200px" data-height="80px" data-backcolor="#0f0" src="' +
        location.hostname + '/template/js/block.js"&gt;' +
        '&lt;/script&gt;';

    return '<pre>' + jsCode + '</pre>';
}

/* hide element */
function closeModal() {
    document.getElementById('code-modal').style.display = "none"
}

/* highlight active menu element */
function highLightElem() {
    let items = document.getElementsByClassName("menu-item");
    [].forEach.call(items, function (element) {

        let location = window.location.href;
        let link = element.firstChild.href;
        if(location == link) {
            element.className += ' active';
        }
    });
}

/* fill table */
function printTable(data, colTitles) {
    /* clean table */
    tHead.innerHTML = '';
    tBody.innerHTML = '';

    /* fill table head */
    let row = document.createElement('tr');
    row.className = "row";
    let th;

    for (let title of colTitles.values()) {
        th = document.createElement('th');
        th.innerText = title;
        row.appendChild(th);
    }
    tHead.appendChild(row);

    /* fill table body */
    data.forEach(function(item) {
        let row = document.createElement('tr');
        row.className = "row";
        let col;

        for (let title of colTitles.keys()) {
            col = document.createElement('td');
            col.innerText = item[title];
            row.appendChild(col);
        }

        tBody.appendChild(row);
    });
}

/* create new table according with new values */
function filter() {
    window.result = table.filter(checkValue);

    let group = document.getElementById('group');
    if (group !== null) {

        if (group.options[group.selectedIndex].value === 'no') {
            printTable(window.result, allCols);
        }

    } else {
        printTable(window.result, allCols);
    }
}

/* group table cols */
function group() {
    let group = document.getElementById('group');
    if (group !== null) {

        let groupVal = group.options[group.selectedIndex].value;
        if (groupVal !== 'no') {
            let groupedRes, obj, shows, clicks;

            /* create array with duplicated values */
            groupedRes = new Map();
            result.forEach(function (elem) {
                if (groupedRes.has("" + elem[groupVal])) {
                    shows = Number(groupedRes.get("" + elem[groupVal])[0]);
                    shows+= Number(elem['shows']);

                    clicks = Number(groupedRes.get("" + elem[groupVal])[1]);
                    clicks+= Number(elem['clicks']);

                    groupedRes.set(elem[groupVal], [shows, clicks]);
                } else {
                    groupedRes.set(elem[groupVal], [Number(elem['shows']), Number(elem['clicks'])]);
                }
            });

            /* create grouped array of objects */
            let newRes = [];
            groupedRes.forEach((value, key) => {
                obj = {};

                obj[groupVal] = key;
                obj['shows'] = value[0];
                obj['clicks'] = value[1];

                newRes.push(obj);
            });

            /* set new columns titles */
            let colsTitles = new Map([
                [groupVal, allCols.get(groupVal)],
                ['shows', allCols.get('shows')],
                ['clicks', allCols.get('clicks')],
            ]);

            printTable(newRes, colsTitles);
        }

    }

    return true;
}

/* check input value according table values */
function checkValue(val) {
    /*
     * check min date
     */
    let minDateCheck;

    let minYear = document.getElementById('min_year'),
        minYearVal = minYear.options[minYear.selectedIndex].value,

        minMonth = document.getElementById('min_month'),
        minMonthVal = minMonth.options[minMonth.selectedIndex].value,

        minDay= document.getElementById('min_day'),
        minDayVal = minDay.options[minDay.selectedIndex].value,

        minDate = minYearVal + '-' + minMonthVal + '-' + minDayVal;
        minDateCheck = new Date(val.date) > new Date(minDate);

    /*
     * check max date
     */
    let maxDateCheck;

    let maxYear = document.getElementById('max_year'),
        maxYearVal = maxYear.options[maxYear.selectedIndex].value,

        maxMonth = document.getElementById('max_month'),
        maxMonthVal = maxMonth.options[maxMonth.selectedIndex].value,

        maxDay= document.getElementById('max_day'),
        maxDayVal = maxDay.options[maxDay.selectedIndex].value,

        maxDate = maxYearVal + '-' + maxMonthVal + '-' + maxDayVal;

        maxDateCheck = new Date(val.date) < new Date(maxDate);

    return minDateCheck && maxDateCheck;
}