<?php

class Topic
{
    /**
     * @return array of topic items
     */
    public static function getTopicList() {
        $db = Db::getConnection();
        
        $result = $db->prepare('
            SELECT *
                FROM topic 
                ORDER BY id ASC');

        try {
            $result->execute();
        } catch (PDOException $e) {
            echo "Select failed!";

            error_log($e->getMessage(), 0);
        }

        $topicList = $result->fetchAll();

        return $topicList;
    }
}
