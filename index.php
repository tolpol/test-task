<?php

error_reporting(E_ALL);

$dirname = __DIR__ . '/log';
if (!is_dir($dirname)) {
    mkdir($dirname);
}

ini_set('error_log', $dirname.'/php-errors.log');

define('ROOT', dirname(__FILE__));
require_once(ROOT.'/components/Router.php');
require_once(ROOT.'/components/Db.php');

$router = new Router();
$router->run();

