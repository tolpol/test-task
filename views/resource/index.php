<?php require_once(ROOT . '/views/header.php'); ?>

    <table class="table table-striped table-responsive">
        <thead>
        <tr class="row">
            <th class="col-lg-1">id</th>
            <th class="col-lg-3">url</th>
            <th class="col-lg-1">Тематика</th>
            <th class="col-lg-2">Контакты</th>
            <th class="col-lg-3">Рекламные блоки</th>
            <th class="col-lg-2">
                <a class="btn btn-success pull-right" href="/resource/new">new</a>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($resourceList as $resourceItem): ?>
            <tr class="row">
                <td class="col-lg-1"><?php echo $resourceItem['id']; ?></td>
                <td class="col-lg-3"><?php echo $resourceItem['url']; ?></td>
                <td class="col-lg-1"><?php echo $resourceItem['topic']; ?></td>
                <td class="col-lg-2"><?php echo $resourceItem['contact']; ?></td>
                <td class="col-lg-3">
                <?php if (!empty($resourceItem['ads_list'])): ?>
                    <?php
                        $ads_list = $resourceItem['ads_list'];
                        $ads_items = explode(',', $ads_list);
                    ?>
                    <select name="" id="" class="form-control">
                    <?php foreach ($ads_items as $ads_item): ?>
                        <option value=""><?php echo $ads_item; ?></option>
                    <?php endforeach; ?>
                    </select>
                <?php endif; ?>
                </td>
                <td class="col-lg-2">
                    <a class="btn btn-danger pull-right delete"
                       href="/resource/delete/<?php echo $resourceItem['id']; ?>" onclick="deleteItem()">delete</a>
                    <a class="btn btn-primary pull-right" href="/resource/edit/<?php echo $resourceItem['id']; ?>">edit</a
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul>
            <?php for ($i = 1; $i <= $pagesCount; $i++): ?>
                <li><a href="/resource/index/<?php echo $i;?>"><?php echo $i?></a></li>
            <?php endfor; ?>
        </ul>
    </div>

<?php require_once(ROOT . '/views/footer.php'); ?>