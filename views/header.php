<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Test task</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="/template/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <header id="header">
        <nav id="menu" class="navbar navbar-dark bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="header-menu">
                            <li class="menu-item"><a href="/">Общая статистика</a>
                            <li class="menu-item"><a href="/resource/index/1">Интернет ресурсы</a></li>
                            <li class="menu-item"><a href="/ads/index">Рекламные блоки</a></li>
                            <li class="menu-item"><a href="/show/index">Статистика показов</a></li>
                            <li class="menu-item"><a href="/click/index">Статистика кликов</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- end div#logo -->
    </header>
    <!-- end div#header -->

            <!-- end div#menu -->
            <main id="page">
                <section id="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">