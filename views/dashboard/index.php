<?php require_once(ROOT . '/views/header.php'); ?>

    <table class="table table-condensed">
        <caption>Фильтры</caption>
        <thead>
        </thead>
        <tbody>
            <tr class="row">
                <td class="col-lg-1">
                    <label for="min_year">год от:</label>
                    <select name="" id="min_year" class="form-control" onchange="filter();group();"></select>
                    <label for="min_month">месяц от:</label>
                    <select name="" data-name="min" id="min_month" class="form-control" onchange="filter();group();setDays(this);"></select>
                    <label for="min_day">день от:</label>
                    <select name="" id="min_day" class="form-control" onchange="filter();group();"></select>
                </td>
                <td class="col-lg-1">
                    <label for="max_year">год до:</label>
                    <select name="" id="max_year" class="form-control" onchange="filter();group();"></select>
                    <label for="max_month">месяц до:</label>
                    <select name="" data-name="max" id="max_month" class="form-control" onchange="filter();group();setDays(this);"></select>
                    <label for="max_day">день до:</label>
                    <select name="" id="max_day" class="form-control" onchange="filter();group();"></select>
                </td>

                <td class="col-lg-2">
                    <label for="group">Группировка по:</label>
                    <select name="group" id="group" class="form-control" onchange="filter();group();">
                        <option value="date" selected>дате</option>
                        <option value="resource_id">ID интернет ресурса</option>
                        <option value="ads_id">ID рекламного блока</option>
                        <option value="country_code">ISO коду страны</option>
                        <option value="no">Не группировать</option>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-striped table-responsive" id="common-statistic">
        <thead id="t-head"></thead>
        <tbody id="t-body"></tbody>
    </table>

<?php if (isset($summaryList)): ?>
    <script type="text/javascript">
         window.table = <?php echo json_encode($summaryList); ?>;
    </script>
<?php endif; ?>

<?php require_once(ROOT . '/views/footer.php'); ?>