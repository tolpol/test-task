<?php

include_once ROOT . '/models/Show.php';

class ShowController
{
    /**
     * create new click item
     * @return bool
     * @throws exception
     */
    public function actionNew()
    {
        if (isset($_POST['resource_id'])) {
            $show = array();

            $resourceId = FormHelper::clean($_POST['resource_id']);
            $ads = FormHelper::clean($_POST['ads_id']);
            $ip = FormHelper::clean($_POST['user_ip']);
            $code = FormHelper::clean($_POST['country_code']);

            if(empty($resourceId) || empty($ads) || empty($ip) || empty($code)) {
                exit;
            }

            if(FormHelper::checkLength($resourceId, 1, 11) && FormHelper::checkLength($ads, 1, 11) &&
                FormHelper::checkLength($ip, 10, 255) && FormHelper::checkLength($code, 2, 13)) {

                $show['resource_id'] = $resourceId;
                $show['ads_id'] = $ads;
                $show['user_ip'] = $ip;
                $show['country_code'] = $code;

                Show::insert($show);
            }
        }

        return true;
    }


    /**
     * display show list
     * @return bool
     */
    public function actionIndex()
    {
        $showList = array();
        $showList = Show::getShowList();

        require_once(ROOT . '/views/show/index.php');

        return true;
    }

}