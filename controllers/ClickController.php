<?php

include_once ROOT . '/models/Click.php';
include_once ROOT . '/services/FormHelper.php';

class ClickController
{
    /**
     * create new click item
     * @return bool
     * @throws exception
     */
    public function actionNew()
    {
        if (isset($_POST['resource_id'])) {
            $click = array();

            $resourceId = FormHelper::clean($_POST['resource_id']);
            $ads = FormHelper::clean($_POST['ads_id']);
            $ip = FormHelper::clean($_POST['user_ip']);
            $code = FormHelper::clean($_POST['country_code']);

            if(empty($resourceId) || empty($ads) || empty($ip) || empty($code)) {
                exit;
            }

            if(FormHelper::checkLength($resourceId, 1, 11) && FormHelper::checkLength($ads, 1, 11) &&
                FormHelper::checkLength($ip, 10, 255) && FormHelper::checkLength($code, 2, 13)) {

                $click['resource_id'] = $resourceId;
                $click['ads_id'] = $ads;
                $click['user_ip'] = $ip;
                $click['country_code'] = $code;

                Click::insert($click);
            }

            exit;
        }

        return true;
    }

    /**
     * show click list
     * @return bool
     */
    public function actionIndex()
    {
        $clickList = array();
        $clickList = Click::getClickList();

        require_once(ROOT . '/views/click/index.php');

        return true;
    }
}