<?php

include_once ROOT . '/models/Dashboard.php';

class DefaultController
{
    /**
     * display summary list
     * @return bool
     */
    public function actionIndex()
    {
        $summaryList = array();
        $summaryList = Dashboard::getCommonStatistics();

        require_once(ROOT . '/views/dashboard/index.php');

        return true;
    }
}