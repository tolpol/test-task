<?php

include_once ROOT . '/models/Resource.php';
include_once ROOT . '/models/Topic.php';
include_once ROOT . '/services/FormHelper.php';

class ResourceController
{
    const ITEMS_PER_PAGE = 15;

    /**
     * redirect to index
     */
    private function redirectToIndex()
    {
        header('Location: /resource/index/1');
    }

    /**
     * add new item
     * @return bool
     */
    public function actionNew()
    {
        if (!isset($_POST['submit'])) {
            $topics = array();
            $topics = Topic::getTopicList();
        } else {
            $resource = array();

            $url = FormHelper::clean($_POST['url']);
            $topic = FormHelper::clean($_POST['topic']);
            $contact = FormHelper::clean($_POST['contact']);

            if(!empty($url) && !empty($topic) && !empty($contact)) {
                $validateUrl = filter_var($url, FILTER_VALIDATE_URL);

                if(FormHelper::checkLength($validateUrl, 6, 255) && FormHelper::checkLength($topic, 1, 11) &&
                    FormHelper::checkLength($contact, 6, 255)) {

                    $resource['url'] = $validateUrl;
                    $resource['topic'] = $topic;
                    $resource['contact'] = $contact;

                    Resource::insert($resource);

                    $this->redirectToIndex();
                } else {
                    echo "Введенные данные некорректны";
                }
            } else {
                echo "Заполните пустые поля";
            }

            exit;
        }

        require_once(ROOT . '/views/resource/new.php');

        return true;
    }

    /**
     * edit item
     * @param int $id
     * @return bool
     */
    public function actionEdit($id)
    {
        if ($id) {
            if (isset($_POST['submit'])) {
                $resource = array();

                $id = FormHelper::clean($id);
                $url = FormHelper::clean($_POST['url']);
                $topic = FormHelper::clean($_POST['topic']);
                $contact = FormHelper::clean($_POST['contact']);

                if(empty($id) || empty($url) || empty($topic) || empty($contact)) {
                    echo "Заполните пустые поля";

                    exit;
                }

                $validateUrl = filter_var($url, FILTER_VALIDATE_URL);

                if(FormHelper::checkLength($id, 1, 11) && FormHelper::checkLength($validateUrl, 6, 255) &&
                    FormHelper::checkLength($topic, 1, 11) && FormHelper::checkLength($contact, 6, 255)) {

                    $resource['id'] = $id;
                    $resource['url'] = $validateUrl;
                    $resource['topic'] = $topic;
                    $resource['contact'] = $contact;

                    Resource::update($resource);

                    $this->redirectToIndex();
                } else {
                    echo "Введенные данные некорректны";
                }

                exit;
            }

            $topics = array();
            $topics = Topic::getTopicList();

            $resourceItem = Resource::getResourceItemById($id);

            require_once(ROOT . '/views/resource/edit.php');
        }

        return true;
    }

    /**
     * remove item
     * @return bool
     */
    public function actionDelete($id)
    {
        if ($id) {
            Resource::delete($id);

            $this->redirectToIndex();
            exit;
        }

        return true;
    }

    /**
     * show resource list
     * @return bool
     */
    public function actionIndex($page)
    {
        $offset = ($page - 1) * self::ITEMS_PER_PAGE;

        $resources = array();
        $resources = Resource::getResourceList();

        $count = count($resources);
        $pagesCount = ceil($count / self::ITEMS_PER_PAGE);

        $resourceList = array();

        $maxOffset = $offset + self::ITEMS_PER_PAGE;
        $limit =  $maxOffset <= $count ? $maxOffset : $count;
        for ($i = $offset; $i < $limit; $i++) {
            $resourceList[] = $resources[$i];
        }

        require_once(ROOT . '/views/resource/index.php');

        return true;
    }
}