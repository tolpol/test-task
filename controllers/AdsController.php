<?php

include_once ROOT . '/models/Resource.php';
include_once ROOT . '/models/Ads.php';
include_once ROOT . '/services/FormHelper.php';

class AdsController
{
    /**
     * redirect to index
     */
    private function redirectToIndex()
    {
        header('Location: /ads/index');
    }

    /**
     * create new ads item
     * @return bool
     */
    public function actionNew()
    {
        if (!isset($_POST['submit'])) {
            $resources = array();
            $resources = Resource::getResourceIds();
        } else {
            $ads = array();

            $title = FormHelper::clean($_POST['title']);
            $resourceId = FormHelper::clean($_POST['resource_id']);
            $description = FormHelper::clean($_POST['description']);

            if(empty($title) || empty($resourceId) || empty($description)) {
                echo "Заполните пустые поля";

                exit;
            }

            if(FormHelper::checkLength($title, 2, 255) && FormHelper::checkLength($resourceId, 1, 11) &&
                FormHelper::checkLength($description, 6, 1000)) {

                $ads['title'] = $title;
                $ads['resource_id'] = $resourceId;
                $ads['description'] = $description;

                Ads::insert($ads);

                $this->redirectToIndex();
            } else {
                echo "Введенные данные некорректны";
            }

            exit;
        }

        require_once(ROOT . '/views/ads/new.php');

        return true;
    }

    /**
     * edit ads item by id
     * @return bool
     */
    public function actionEdit($id)
    {
        if ($id) {
            if (isset($_POST['submit'])) {
                $ads = array();

                $id = FormHelper::clean($id);
                $title = FormHelper::clean($_POST['title']);
                $resourceId = FormHelper::clean($_POST['resource_id']);
                $description = FormHelper::clean($_POST['description']);

                if(empty($id) || empty($title) || empty($resourceId) || empty($description)) {
                    echo "Заполните пустые поля";

                    exit;
                }

                if(FormHelper::checkLength($id, 1, 11) && FormHelper::checkLength($title, 2, 255) &&
                    FormHelper::checkLength($resourceId, 1, 11) && FormHelper::checkLength($description, 6, 1000)) {

                    $ads['id'] = $id;
                    $ads['title'] = $title;
                    $ads['resource_id'] = $resourceId;
                    $ads['description'] = $description;

                    Ads::update($ads);

                    $this->redirectToIndex();
                } else {
                    echo "Введенные данные некорректны";
                }

                exit;
            }

            $adsItem = Ads::getAdsItemById($id);

            $resources = array();
            $resources = Resource::getResourceIds();

            require_once(ROOT . '/views/ads/edit.php');
        }

        return true;
    }

    /**
     * remove ads item by id
     * @return bool
     */
    public function actionDelete($id)
    {
        if ($id) {
            Ads::delete($id);

            $this->redirectToIndex();

            exit;
        }

        return true;
    }

    /**
     * show ads list
     * @return bool
     */
    public function actionIndex()
    {
        $adsList = array();
        $adsList = Ads::getAdsList();

        require_once(ROOT . '/views/ads/index.php');

        return true;
    }
}