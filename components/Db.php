<?php

class Db
{
    private static $conn;

    public static function getConnection()
    {
        $paramsPath = ROOT . '/config/db_params.php';
        $params = include($paramsPath);

        if (!empty(self::$conn)){
            return self::$conn;
        }

        try {
            $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
            $db = new PDO($dsn, $params['user'], $params['password'], array(PDO::ATTR_PERSISTENT => true));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->exec("set names utf8");

            self::$conn = $db;

            return $db;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();

            error_log($e->getMessage(), 0);

            die();
        }
    }
}
